from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Contenido

# Create your views here.

from django.views.decorators.csrf import csrf_exempt  # Saltar mecanismo de seguridad CSRF


# Create your views here.


def index(request):
    return HttpResponse("<h1>Este es mi primer CMS</h1>")


formulario = '<form method="POST">' \
             '<label>Introduce el recurso para guardar:' \
             '<input name="name" autocomplete="name"/>' \
             '</label>' \
             '<button>Enviar</button>' \
             '</form>'

def save(request, recurso):
    Valor = request.body.decode()
    contenido = Contenido(clave=recurso, valor=Valor)
    contenido.save()
    return contenido

@csrf_exempt  # Me salto mecanismo de seguridad
def get_resource(request, recurso):
    try:
        contenido = Contenido.objects.get(clave=recurso)
        if request.method == "PUT":
            contenido = save(request, recurso)
        if request.method == "POST":
            contenido = save(request, recurso)
        response = "El recurso pedido es: " + contenido.clave + ". Su valor es: " + contenido.valor \
                   + ".Su identificador es: " + str(contenido.id)
        return HttpResponse(response)

    except Contenido.DoesNotExist:
        if request.method == "POST":
            contenido = save(request, recurso)
            response = "El recurso " + contenido.clave + " se ha guardado con valor " + contenido.valor \
                       + ". Su identificador es: " + str(contenido.id)
            return HttpResponse(response)
        else:
            return HttpResponse(formulario)

